/**
 * Bài 1
 * Đầu vào: Số ngày làm 
 * Phương pháp tính: Tổng số ngày làm * 100.000
 * Đầu ra: Tổng số tiền lương 
 */
var ngayLam = 2;
var tongLuong = ngayLam * 100000;
console.log("Tổng lương = ",tongLuong);

/**
 * Bài 2
 * Đầu vào: Các số thực 
 * Phương pháp tính: Tổng tất cả các chữ số chia 5
 * Đầu ra: Trung bình của các số thực
 */
var a = 1, b = 2, c = 3, d = 4, e = 5;
var trungbinhTong = (a+b+c+d+e)/5;
console.log("Trung bình tổng = ",trungbinhTong);

/**
 * Bài 3
 * Đầu vào:Số tiền USD
 * Phương pháp tính:Số tiền USD * 23500
 * Đầu ra: Số tiền sau khi quy đổi
 */
var soTien = 10;
var tongTien = soTien * 23500;
console.log("Tổng tiền = ",tongTien, " VND");

/**
 * Đầu vào: Chiều dài và chiều rộng hình chữ nhật
 * Phương pháp tính: 
 * + Đối với chu vi: (Chiều dài + chiều rộng ) * 2
 * + Đối với diện tích: Chiều dài * chiều rộng
 * Đầu ra: Chu vi và diện tích hình chữ nhật
 */
var width = 20, height = 8;
var chuVi= (width + height) *2;
var dienTich = width * height;
console.log("Chu vi hình chữ nhật = ", chuVi);
console.log("Diện tích hình chữ nhật = ", dienTich);

/**
 * Đầu vào: Số có 2 chữ số
 * Phương pháp tính: 
 * + Chia lấy dư chữ số hàng đơn vị gán với 1 giá trị: n % 10
 * + Chia lấy thương chữ số hàng đơn vị gán với 1 giá trị: n / 10
 * + Lấy giá trị ở hàng đơn vị + giá trị ở hàng chục
 * Đầu ra: Tổng của số có 2 chữ số
 */
var n = 38;
var n_don_vi = n % 10;
var n_hang_chuc = Math.floor(n / 10);
var sum = n_don_vi + n_hang_chuc;
console.log("Tổng của số có 2 chữ số  = ", sum); 